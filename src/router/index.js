import Vue from 'vue'
import Router from 'vue-router'
import AppToPhone from '../components/AppToPhone'
import AppToApp from '../components/AppToApp'
import PhoneToApp from '../components/PhoneToApp'
import Home from '../components/Home'
import Agent from '../components/Agent'
import CallReceiver from '../components/CallReceiver'
Vue.use(Router);

const routes = [
    {
      path: '/',
      component: Home
    },
    {
      path: '/get-call-from-phone-to-app',
      component: PhoneToApp

    },

    {
      path: '/agent/:id',
      component: Agent
    },
    {
      path: '/call-app-to-phone',
      component: AppToPhone
    },
    {
      path: '/call-receiver',
      component: CallReceiver
    },
    {
      path: '/call-app-to-app',
      component: AppToApp
    }
  ]
  
  export default new Router({
    routes,
    mode: "history"
  })