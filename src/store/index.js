import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex)

export default new Vuex.Store({
    //app1, app2: for app to app calling
    //agent1, agent2: for listening to call from call center
    state: {
        numberToCall: "84942906209",
        common_key: {
            name: "common_key",
            key: ""
        },
        app_receiver: {
            name: "app_receiver",
            key: ""
        },
        app_0001: {
            name: "app_0001",
            key: ""
        },
        app_0002: {
            name: "app_0002",
            key: ""
        },
        agent_0001: {
            name: 'agent_0001',
            key: ""
        },
        agent_0002: {
            name: 'agent_0002',
            key: ""
        }


    },
    mutations: {
        addChar(state, char) {
            state.numberToCall += char;
        },

        app_0001(state, data) {
            console.log('commiting data for app_0001');
            state.app_0001.key = data;
        },
        app_0002(state, data) {
            console.log('commiting data for app_0002');
            console.log('data for app_0002', data)
            state.app_0002.key = data;
        },

        agent_0001(state, data) {
            console.log('commiting data for agent_0001');
            console.log('data for agent_0001', data);
            state.agent_0001.key = data;
        },

        agent_0002(state, data) {
            console.log('commiting data for agent_0002');
            console.log('data for agent_0002', data);
            state.agent_0002.key = data;
        },

        app_receiver(state, data) {
            state.app_receiver.key = data;
        },

        common_key(state, data) {
            state.common_key.key = data;
        },

        removeLastChar(state) {
            state.numberToCall = state.numberToCall.substr(0, state.numberToCall.length - 1);
        }
    },

    getters: {
        numberToCall: state => state.numberToCall
    },

    plugins: [createPersistedState()]

})